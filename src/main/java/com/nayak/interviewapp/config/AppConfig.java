package com.nayak.interviewapp.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component
public class AppConfig {

    @Value("${account.initial-credit}")
    private String initialCreditAmount;
}
