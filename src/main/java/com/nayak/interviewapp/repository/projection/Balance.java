package com.nayak.interviewapp.repository.projection;

import java.math.BigDecimal;

public interface Balance {

    BigDecimal getBalance();

    String getCurrencyCode();
}
