package com.nayak.interviewapp.repository;

import com.nayak.interviewapp.model.Account;
import com.nayak.interviewapp.model.Transaction;
import com.nayak.interviewapp.repository.projection.Balance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    Page<Transaction> findByAccount(Account account, Pageable pageable);

    @Query(value = "SELECT SUM(TRANSACTION_AMOUNT) as balance" +
            "            , currency as currencyCode " +
            "         FROM TRANSACTION  " +
            "        WHERE account_id=:accountId " +
            "     GROUP BY currency", nativeQuery = true)
    List<Balance> findBalance(@Param("accountId") Long accountId);
}
