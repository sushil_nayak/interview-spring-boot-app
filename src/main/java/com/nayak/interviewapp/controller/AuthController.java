package com.nayak.interviewapp.controller;


import com.nayak.interviewapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@CrossOrigin("*")
@RestController
@RequiredArgsConstructor
public class AuthController {

    private final UserService userService;

    @PostMapping("/login")
    public ResponseEntity<Map<String, String>> login() {
        String jwtToken = userService.createUser();

        Map<String, String> result = new HashMap<>();
        result.put("accessToken", jwtToken);

        return ResponseEntity.ok(result);
    }
}
