package com.nayak.interviewapp.controller;

import com.nayak.interviewapp.model.Transaction;
import com.nayak.interviewapp.model.dto.TransactionDto;
import com.nayak.interviewapp.repository.projection.Balance;
import com.nayak.interviewapp.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @GetMapping("/balance")
    public ResponseEntity<List<Balance>> balance() {
        return ResponseEntity.ok(accountService.getAccountBalance());
    }

    // UI user would prefer page response instead of List..
    @GetMapping("/transactions")
    public ResponseEntity<Page<Transaction>> transactions(Pageable pageable) {
        return ResponseEntity.ok(accountService.getTransactions(pageable));
    }

    @PostMapping("/spend")
    public ResponseEntity<Void> debitTransaction(@RequestBody TransactionDto transactionDto) {
        accountService.saveTransaction(transactionDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
