package com.nayak.interviewapp.model.dto;

import com.nayak.interviewapp.model.Currency;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TransactionDto {
    private LocalDate date;
    private String description;
    private BigDecimal amount;
    private Currency currency;
}
