package com.nayak.interviewapp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "acct_seq_gen")
    @SequenceGenerator(name = "acct_seq_gen", sequenceName = "acct_seq", allocationSize = 1)
    private Long id;

    private String username;

    @OneToMany(mappedBy = "account", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<Transaction> transactions = new ArrayList<>();

    public Account(String username) {
        this.username = username;
    }
}
