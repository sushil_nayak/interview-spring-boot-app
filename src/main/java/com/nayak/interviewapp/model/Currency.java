package com.nayak.interviewapp.model;

public enum Currency {
    GBP,
    USD,
    EUR;
}
