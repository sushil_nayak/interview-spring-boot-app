package com.nayak.interviewapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trans_seq_gen")
    @SequenceGenerator(name = "trans_seq_gen", sequenceName = "trans_seq", allocationSize = 1)
    private Long id;

    private BigDecimal transactionAmount;

    private LocalDate transactionDate;

    private String description;

    @Enumerated(EnumType.STRING)
    private Currency currency;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "account_id")
    @JsonIgnore
    private Account account;

    public Transaction(Account account, BigDecimal transactionAmount, String description, Currency currency, LocalDate transactionDate) {
        this.transactionAmount = transactionAmount;
        this.transactionDate = transactionDate;
        this.description = description;
        this.currency = currency;
        this.account = account;
    }
}
