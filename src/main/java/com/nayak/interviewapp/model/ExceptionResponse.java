package com.nayak.interviewapp.model;

import lombok.Getter;

import java.time.LocalDateTime;


@Getter
public class ExceptionResponse {
    private String message;
    private String detail;
    private LocalDateTime timestamp = LocalDateTime.now();

    public ExceptionResponse(String message, String detail) {
        this.message = message;
        this.detail = detail;
    }
}
