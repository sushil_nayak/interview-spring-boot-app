package com.nayak.interviewapp.service;

import com.nayak.interviewapp.config.AppConfig;
import com.nayak.interviewapp.config.jwt.JwtTokenUtil;
import com.nayak.interviewapp.model.Account;
import com.nayak.interviewapp.model.Transaction;
import com.nayak.interviewapp.model.User;
import com.nayak.interviewapp.repository.AccountRepository;
import com.nayak.interviewapp.repository.TransactionRepository;
import com.nayak.interviewapp.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;

import static com.nayak.interviewapp.model.Currency.GBP;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;
    private final JwtTokenUtil jwtTokenUtil;

    private final AppConfig appConfig;

    @Transactional
    public String createUser() {
        User newUser = userRepository.save(new User());
        Account account = accountRepository.save(new Account(newUser.getUsername()));
        transactionRepository.save(new Transaction(account, new BigDecimal(appConfig.getInitialCreditAmount()), "Initial Credit", GBP, LocalDate.now()));
        return jwtTokenUtil.generateAccessToken(newUser);
    }
}
