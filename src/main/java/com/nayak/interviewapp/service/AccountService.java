package com.nayak.interviewapp.service;

import com.nayak.interviewapp.model.Account;
import com.nayak.interviewapp.model.Transaction;
import com.nayak.interviewapp.model.User;
import com.nayak.interviewapp.model.dto.TransactionDto;
import com.nayak.interviewapp.repository.AccountRepository;
import com.nayak.interviewapp.repository.TransactionRepository;
import com.nayak.interviewapp.repository.projection.Balance;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountService {

    private final TransactionRepository transactionRepository;
    private final AccountRepository accountRepository;

    public List<Balance> getAccountBalance() {
        return getCurrentUser()
                .map(user -> transactionRepository.findBalance(user.getId()))
                .orElseThrow(() -> new UsernameNotFoundException("User not found!"));
    }

    // UI user would prefer page response instead of List..
    @Transactional(readOnly = true)
    public Page<Transaction> getTransactions(Pageable pageable) {
        return getCurrentUser()
                .map(u -> accountRepository.findByUsername(u.getUsername())
                        .map(user -> transactionRepository
                                .findByAccount(user, pageable))
                        .orElseThrow(() -> new UsernameNotFoundException("Account not found")))
                .orElseThrow(() -> new UsernameNotFoundException("User not found!"));
    }

    @Transactional
    public void saveTransaction(TransactionDto transactionDto) {
        Account account = getCurrentUser()
                .map(u -> accountRepository
                        .findByUsername(u.getUsername())
                        .orElseThrow(() -> new UsernameNotFoundException("Account not found")))
                .orElseThrow(() -> new UsernameNotFoundException("User not found!"));

        transactionRepository.save(new Transaction(account,
                transactionDto.getAmount().multiply(new BigDecimal("-1")),
                transactionDto.getDescription(),
                transactionDto.getCurrency(),
                transactionDto.getDate()));

    }


    public Optional<User> getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Optional<User> user = Optional.empty();
        if (authentication != null && authentication.getPrincipal() instanceof User) {
            user = Optional.of((User) authentication.getPrincipal());

        }
        return user;
    }

}
