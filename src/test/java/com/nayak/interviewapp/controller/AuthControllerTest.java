package com.nayak.interviewapp.controller;

import com.nayak.interviewapp.config.jwt.JwtTokenUtil;
import com.nayak.interviewapp.repository.UserRepository;
import com.nayak.interviewapp.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = AuthController.class)
//@ContextConfiguration(classes = {SecurityConfig.class, JwtTokenFilter.class, JwtTokenUtil.class, JwtConfig.class})
//@SpringBootTest
@AutoConfigureMockMvc
class AuthControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    UserService userService;
    @MockBean
    UserRepository userRepository;
    @MockBean
    JwtTokenUtil jwtTokenUtil;

    @Test
    void authControllerShouldProvideTokenInvokedTest() throws Exception {

        when(userService.createUser()).thenReturn("abcd-token");

        mockMvc.perform(post("/login"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.accessToken").exists())
                .andExpect(jsonPath("$.accessToken").value("abcd-token"))
        ;
    }
}
