package com.nayak.interviewapp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import javax.persistence.EntityManager;
import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {
        "account.initial-credit=3000"
})
public class AuthControllerIT {

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    EntityManager entityManager;


    @Test
    void loginShouldProvideAuthorizationHeaderWithTokenTest() {
        ParameterizedTypeReference<HashMap<String, String>> responseType = new ParameterizedTypeReference<HashMap<String, String>>() {
        };

        RequestEntity<Void> request = RequestEntity.post("/login").accept(MediaType.APPLICATION_JSON).build();
        ResponseEntity<HashMap<String, String>> responseEntity = restTemplate.exchange(request, responseType);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).containsKey("accessToken");
        assertThat(responseEntity.getBody().get("accessToken")).hasSize(260);
    }

}
