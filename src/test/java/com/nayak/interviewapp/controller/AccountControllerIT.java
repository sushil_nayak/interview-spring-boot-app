package com.nayak.interviewapp.controller;

import com.nayak.interviewapp.model.Currency;
import com.nayak.interviewapp.model.Transaction;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {
        "account.initial-credit=3000"
})
class AccountControllerIT {

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    EntityManager entityManager;


    @Test
    void balanceGetFetchedSuccessfullyTest() {
        String accessToken = getAccessToken();

        ParameterizedTypeReference<List<Map<String, Object>>> responseType = new ParameterizedTypeReference<List<Map<String, Object>>>() {
        };
        RequestEntity<Void> request = RequestEntity.get("/balance").header("Authorization", "Bearer " + accessToken).accept(MediaType.APPLICATION_JSON).build();
        ResponseEntity<List<Map<String, Object>>> responseEntity = restTemplate.exchange(request, responseType);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).hasSize(1);
        assertThat(responseEntity.getBody().get(0).get("balance")).isEqualTo(3000.0);
        assertThat(responseEntity.getBody().get(0).get("currencyCode")).isEqualTo("GBP");

    }

    @Test
    @Transactional
    void spendGetsPostedSuccessfullyTest() {
        entityManager.createQuery("delete from Transaction").executeUpdate();
        entityManager.flush();

        String accessToken = getAccessToken();

        RequestEntity<String> request = RequestEntity.post("/spend")
                .header("Authorization", "Bearer " + accessToken)
                .contentType(MediaType.APPLICATION_JSON).body("{\n" +
                        "  \"date\": \"2021-12-18\",\n" +
                        "  \"description\": \"H&M\",\n" +
                        "  \"amount\": 123.44,\n" +
                        "  \"currency\": \"GBP\"\n" +
                        "}");
        ResponseEntity<Void> responseEntity = restTemplate.exchange(request, Void.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        List<Transaction> resultList = entityManager.createQuery("select s from Transaction s order by s.id desc").getResultList();

        assertThat(resultList).hasSize(2);
        assertThat(resultList.get(0).getDescription()).isEqualTo("H&M");
        assertThat(resultList.get(0).getCurrency()).isEqualTo(Currency.GBP);
        assertThat(resultList.get(0).getTransactionAmount()).isEqualTo(new BigDecimal("-123.44"));
        assertThat(resultList.get(0).getTransactionDate()).isEqualTo(LocalDate.of(2021, Month.DECEMBER, 18));
    }

    @Test
    void transactionsTest() {
        String accessToken = getAccessToken();

        RequestEntity<String> request = RequestEntity.post("/spend")
                .header("Authorization", "Bearer " + accessToken)
                .contentType(MediaType.APPLICATION_JSON).body("{\n" +
                        "  \"date\": \"2021-12-18\",\n" +
                        "  \"description\": \"H&M\",\n" +
                        "  \"amount\": 123.44,\n" +
                        "  \"currency\": \"GBP\"\n" +
                        "}");
        ResponseEntity<Void> responseEntity = restTemplate.exchange(request, Void.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);


        ParameterizedTypeReference<Map<String, Object>> transactionResponseType = new ParameterizedTypeReference<Map<String, Object>>() {
        };
        RequestEntity<Void> transactionRequest = RequestEntity.get("/transactions").header("Authorization", "Bearer " + accessToken).accept(MediaType.APPLICATION_JSON).build();
        ResponseEntity<Map<String, Object>> transactionResponseEntity = restTemplate.exchange(transactionRequest, transactionResponseType);

        assertThat(transactionResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        System.out.println(transactionResponseEntity.getBody());

        List<Map<String, Object>> contentList = (List<Map<String, Object>>) transactionResponseEntity.getBody().get("content");

        assertThat(contentList).hasSize(2);
        assertThat(contentList.get(0)).containsEntry("description", "Initial Credit");
        assertThat(contentList.get(0)).containsEntry("currency", "GBP");
        assertThat(contentList.get(0)).containsEntry("transactionAmount", 3000.0);
        assertThat(contentList.get(0)).containsEntry("transactionDate", LocalDate.now().toString());
        assertThat(contentList.get(1)).containsEntry("description", "H&M");
        assertThat(contentList.get(1)).containsEntry("currency", "GBP");
        assertThat(contentList.get(1)).containsEntry("transactionAmount", -123.44);
        assertThat(contentList.get(1)).containsEntry("transactionDate", "2021-12-18");
    }

    String getAccessToken() {

        ParameterizedTypeReference<HashMap<String, String>> responseType = new ParameterizedTypeReference<HashMap<String, String>>() {
        };
        RequestEntity<Void> request = RequestEntity.post("/login").accept(MediaType.APPLICATION_JSON).build();
        ResponseEntity<HashMap<String, String>> responseEntity = restTemplate.exchange(request, responseType);

        return responseEntity.getBody().get("accessToken");
    }


}
