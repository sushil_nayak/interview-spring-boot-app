package com.nayak.interviewapp.controller;

import com.nayak.interviewapp.model.Account;
import com.nayak.interviewapp.model.Currency;
import com.nayak.interviewapp.model.Transaction;
import com.nayak.interviewapp.model.dto.TransactionDto;
import com.nayak.interviewapp.repository.TransactionRepository;
import com.nayak.interviewapp.repository.UserRepository;
import com.nayak.interviewapp.repository.projection.Balance;
import com.nayak.interviewapp.service.AccountService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class AccountControllerTest {

    @MockBean
    AccountService accountService;

    @MockBean
    UserRepository userRepository;

    @MockBean
    TransactionRepository transactionRepository;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser
    void balanceShouldGetFetchedSuccessfullyTest() throws Exception {

        when(accountService.getAccountBalance()).thenReturn(Arrays.asList(new Balance() {
            @Override
            public BigDecimal getBalance() {
                return new BigDecimal("56785.33");
            }

            @Override
            public String getCurrencyCode() {
                return "EUR";
            }
        }));

        mockMvc.perform(get("/balance"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].balance").value("56785.33"))
                .andExpect(jsonPath("$[0].currencyCode").value("EUR"))
        ;
    }


    @Test
    @WithMockUser
    void transactionShouldGetFetchedSuccessfullyTest() throws Exception {

        ArgumentCaptor<Pageable> pageableCaptor = ArgumentCaptor.forClass(Pageable.class);


        Account account = new Account("nayak");
        when(accountService.getTransactions(any(Pageable.class))).thenReturn(new PageImpl<>(Arrays.asList(
                new Transaction(account, new BigDecimal("12.39"), "Test #1", Currency.GBP, LocalDate.now().minusDays(2)),
                new Transaction(account, new BigDecimal("45.31"), "Test #2", Currency.GBP, LocalDate.now().minusDays(1)),
                new Transaction(account, new BigDecimal("87.13"), "Test #3", Currency.GBP, LocalDate.now())
        )));

        mockMvc.perform(get("/transactions").param("page", "0")
                        .param("size", "10")
                        .param("sort", "transactionDate,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content", hasSize(3)))
                .andExpect(jsonPath("$.content[0].transactionAmount").value("12.39"))
                .andExpect(jsonPath("$.content[1].transactionAmount").value("45.31"))
                .andExpect(jsonPath("$.content[2].transactionAmount").value("87.13"))
                .andExpect(jsonPath("$.content[0].description").value("Test #1"))
                .andExpect(jsonPath("$.content[1].description").value("Test #2"))
                .andExpect(jsonPath("$.content[2].description").value("Test #3"))
        ;

        verify(accountService).getTransactions(pageableCaptor.capture());

        PageRequest pageable = (PageRequest) pageableCaptor.getValue();
        assertThat(pageable.getPageNumber()).isZero();
        assertThat(pageable.getPageSize()).isEqualTo(10);
        assertThat(pageable.getSort().getOrderFor("transactionDate").getDirection()).isEqualTo(Sort.Direction.DESC);

    }

    @Test
    @WithMockUser
    void spendShouldAddDebitRowSuccessfullyTest() throws Exception {

        doNothing().when(accountService).saveTransaction(any(TransactionDto.class));

        mockMvc.perform(post("/spend")
                        .content("{\n" +
                                "  \"date\": \"2021-12-18\",\n" +
                                "  \"description\": \"Testing Spending\",\n" +
                                "  \"amount\": 98763.02,\n" +
                                "  \"currency\": \"GBP\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
        ;

        ArgumentCaptor<TransactionDto> transactionDtoArgumentCaptor = ArgumentCaptor.forClass(TransactionDto.class);

        verify(accountService).saveTransaction(transactionDtoArgumentCaptor.capture());

        assertThat(transactionDtoArgumentCaptor.getValue().getAmount()).isEqualTo(new BigDecimal("98763.02"));
        assertThat(transactionDtoArgumentCaptor.getValue().getCurrency()).isEqualTo(Currency.GBP);
        assertThat(transactionDtoArgumentCaptor.getValue().getDate()).isEqualTo(LocalDate.of(2021, Month.DECEMBER, 18));
        assertThat(transactionDtoArgumentCaptor.getValue().getDescription()).isEqualTo("Testing Spending");
    }

    @Test
    @WithMockUser
    void shouldThrowBadRequestWhenIncorrectCurrencyCodeIsProvidedTest() throws Exception {

        doNothing().when(accountService).saveTransaction(any(TransactionDto.class));

        mockMvc.perform(post("/spend")
                        .content("{\n" +
                                "  \"date\": \"2021-12-18\",\n" +
                                "  \"description\": \"Testing Spending\",\n" +
                                "  \"amount\": 98763.02,\n" +
                                "  \"currency\": \"AAA\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.message", containsString("String \"AAA\": not one of the values accepted for Enum class")))
                .andExpect(jsonPath("$.detail").value("uri=/spend"))
                .andExpect(jsonPath("$.timestamp").exists())
        ;

        verify(accountService, times(0)).saveTransaction(any(TransactionDto.class));

    }


    @Test
    @WithMockUser
    void shouldThrowInternalServerErrorWhenDataAccessRelatedExceptionHappensTest() throws Exception {

        doThrow(new DataAccessResourceFailureException("Something went Wrong!"))
                .when(accountService)
                .saveTransaction(any(TransactionDto.class));

        mockMvc.perform(post("/spend")
                        .content("{\n" +
                                "  \"date\": \"2021-12-18\",\n" +
                                "  \"description\": \"Testing Spending\",\n" +
                                "  \"amount\": 98763.02,\n" +
                                "  \"currency\": \"EUR\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.message").value("Something went Wrong!"))
                .andExpect(jsonPath("$.detail").value("uri=/spend"))
                .andExpect(jsonPath("$.timestamp").exists())
        ;

        verify(accountService, times(1)).saveTransaction(any(TransactionDto.class));
    }


}