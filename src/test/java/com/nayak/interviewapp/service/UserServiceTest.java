package com.nayak.interviewapp.service;

import com.nayak.interviewapp.config.AppConfig;
import com.nayak.interviewapp.config.jwt.JwtTokenUtil;
import com.nayak.interviewapp.model.Account;
import com.nayak.interviewapp.model.User;
import com.nayak.interviewapp.repository.AccountRepository;
import com.nayak.interviewapp.repository.TransactionRepository;
import com.nayak.interviewapp.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    AppConfig appConfig;
    @Mock
    UserRepository userRepository;
    @Mock
    AccountRepository accountRepository;
    @Mock
    TransactionRepository transactionRepository;
    @Mock
    JwtTokenUtil jwtTokenUtil;

    @InjectMocks
    UserService userService;

    ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);

    @Test
    void userShouldGetCreatedSuccessfullyTest() {
        User emptyUser = new User();
        Account emptyAccount = new Account();


        when(appConfig.getInitialCreditAmount()).thenReturn("987.65");
        when(userRepository.save(any(User.class))).thenReturn(emptyUser);
        when(accountRepository.save(any(Account.class))).thenReturn(emptyAccount);
        when(jwtTokenUtil.generateAccessToken(userArgumentCaptor.capture())).thenReturn("test-token-returned");

        String accessToken = userService.createUser();

        assertThat(accessToken).isEqualTo("test-token-returned");
        assertThat(userArgumentCaptor.getValue()).isEqualTo(emptyUser);

        InOrder inOrder = inOrder(userRepository, accountRepository, transactionRepository, jwtTokenUtil);
        inOrder.verify(userRepository, times(1)).save(any());
        inOrder.verify(accountRepository, times(1)).save(any());
        inOrder.verify(transactionRepository, times(1)).save(any());
        inOrder.verify(jwtTokenUtil, times(1)).generateAccessToken(any());
    }
}