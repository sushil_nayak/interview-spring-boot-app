package com.nayak.interviewapp.service;

import com.nayak.interviewapp.model.User;
import com.nayak.interviewapp.model.dto.TransactionDto;
import com.nayak.interviewapp.repository.AccountRepository;
import com.nayak.interviewapp.repository.TransactionRepository;
import com.nayak.interviewapp.repository.projection.Balance;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class AccountServiceTest {

    @Mock
    TransactionRepository transactionRepository;
    @Mock
    AccountRepository accountRepository;
    @InjectMocks
    @Spy
    AccountService accountService;

    @Test
    void accountBalanceShouldGetFetchedSuccessfullyTest() {

        User user = new User();
        ReflectionTestUtils.setField(user, "id", 1L);
        when(accountService.getCurrentUser()).thenReturn(Optional.of(user));
        when(transactionRepository.findBalance(anyLong())).thenReturn(Collections.singletonList(new Balance() {
            @Override
            public BigDecimal getBalance() {
                return new BigDecimal("564.32");
            }

            @Override
            public String getCurrencyCode() {
                return "EUR";
            }
        }));


        List<Balance> balances = accountService.getAccountBalance();

        verify(transactionRepository, times(1)).findBalance(anyLong());
        assertThat(balances).hasSize(1);
        assertThat(balances.get(0).getBalance()).isEqualTo(new BigDecimal("564.32"));
        assertThat(balances.get(0).getCurrencyCode()).isEqualTo("EUR");

    }

    @Test
    void accountBalanceShouldThrowUserNotFoundExceptionWhenNoUserNotFoundTest() {

        UsernameNotFoundException usernameNotFoundException = Assertions.assertThrows(UsernameNotFoundException.class, () -> accountService.getAccountBalance());

        verify(transactionRepository, times(0)).findBalance(anyLong());

        assertThat(usernameNotFoundException.getMessage()).isEqualTo("User not found!");

    }

    @Test
    void fetchingTransactionsShouldThrowUserNotFoundExceptionWhenNoUserNotFoundTest() {

        UsernameNotFoundException usernameNotFoundException = Assertions.assertThrows(UsernameNotFoundException.class, () -> accountService.getTransactions(Pageable.ofSize(2)));

        verify(accountRepository, times(0)).findByUsername(anyString());

        assertThat(usernameNotFoundException.getMessage()).isEqualTo("User not found!");

    }

    @Test
    void savingTransactionShouldThrowUserNotFoundExceptionWhenNoUserNotFoundTest() {

        UsernameNotFoundException usernameNotFoundException = Assertions.assertThrows(UsernameNotFoundException.class, () -> accountService.saveTransaction(new TransactionDto()));

        verify(accountRepository, times(0)).findByUsername(anyString());

        assertThat(usernameNotFoundException.getMessage()).isEqualTo("User not found!");

    }


}