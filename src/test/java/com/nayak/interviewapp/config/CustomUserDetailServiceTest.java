package com.nayak.interviewapp.config;

import com.nayak.interviewapp.model.User;
import com.nayak.interviewapp.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CustomUserDetailServiceTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    CustomUserDetailService customUserDetailService;

    ArgumentCaptor<String> usernameArgumentCaptor = ArgumentCaptor.forClass(String.class);

    @Test
    void userFoundSuccessTest() {
        User user = new User();
        when(userRepository.findByUsername(usernameArgumentCaptor.capture())).thenReturn(Optional.of(user));


        UserDetails userDetails = customUserDetailService.loadUserByUsername("sunita");

        assertThat(usernameArgumentCaptor.getValue()).isEqualTo("sunita");
        assertThat(userDetails.getUsername()).isEqualTo(user.getUsername());
    }

    @Test
    void userNotFoundTest() {
        when(userRepository.findByUsername(usernameArgumentCaptor.capture())).thenReturn(Optional.empty());

        UsernameNotFoundException notFoundException = assertThrows(UsernameNotFoundException.class,
                () -> customUserDetailService.loadUserByUsername("test"));

        assertThat(notFoundException.getMessage()).isEqualTo("User not found!");

    }

}