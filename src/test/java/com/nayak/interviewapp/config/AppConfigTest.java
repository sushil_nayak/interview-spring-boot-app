package com.nayak.interviewapp.config;

import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;


class AppConfigTest {

    @Test
    void initialCreditAmountTest() {
        AppConfig appConfig = new AppConfig();

        ReflectionTestUtils.setField(appConfig, "initialCreditAmount", "999");

        assertThat(appConfig.getInitialCreditAmount()).isEqualTo("999");
    }
}