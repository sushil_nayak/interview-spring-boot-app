package com.nayak.interviewapp.repository;

import com.nayak.interviewapp.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.security.core.userdetails.UserDetails;

import javax.sql.DataSource;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    DataSource dataSource;

    @Autowired
    TestEntityManager entityManager;

    @Test
    void findUserFetchesSuccessfullyTest() {
        User user = new User();
        String username = user.getUsername();
        entityManager.persistAndFlush(user);

        Optional<UserDetails> actualUser = userRepository.findByUsername(username);

        assertThat(actualUser).contains(user);
        assertThat(actualUser.get().getPassword()).isEmpty();
        assertThat(actualUser.get().getAuthorities()).isEmpty();
    }

    @Test
    void findUserFetchesFailTest() {
        Optional<UserDetails> actualUser = userRepository.findByUsername("not-available-user");

        assertThat(actualUser).isNotPresent();
    }
}