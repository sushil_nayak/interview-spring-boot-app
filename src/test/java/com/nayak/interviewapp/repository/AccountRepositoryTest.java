package com.nayak.interviewapp.repository;

import com.nayak.interviewapp.model.Account;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import javax.sql.DataSource;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class AccountRepositoryTest {
    @Autowired
    DataSource dataSource;

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    AccountRepository accountRepository;

    @Test
    void accountFoundTest() {
        Long existingAccountId = entityManager.persistAndGetId(new Account("sunita"), Long.class);

        Optional<Account> account = accountRepository.findByUsername("sunita");

        assertThat(account).isPresent();
        assertThat(account.get().getId()).isEqualTo(existingAccountId);
        assertThat(account.get().getUsername()).isEqualTo("sunita");
        assertThat(account.get().getTransactions()).isEmpty();
    }

    @Test
    void accountFoundFailTest() {
        entityManager.persistAndFlush(new Account("nayak"));

        Optional<Account> account = accountRepository.findByUsername("sunita");

        assertThat(account).isNotPresent();
    }

    @Test
    void accountSaveTest() {
        Account expectedAccount = accountRepository.save(new Account("sunita"));
        accountRepository.flush();

        Account actualAccount = entityManager.find(Account.class, expectedAccount.getId());

        assertThat(actualAccount.getUsername()).isEqualTo("sunita");

    }
}