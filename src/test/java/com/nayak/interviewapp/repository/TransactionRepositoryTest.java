package com.nayak.interviewapp.repository;

import com.nayak.interviewapp.model.Account;
import com.nayak.interviewapp.model.Currency;
import com.nayak.interviewapp.model.Transaction;
import com.nayak.interviewapp.repository.projection.Balance;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class TransactionRepositoryTest {

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    DataSource dataSource;

    @Autowired
    TestEntityManager entityManager;

    @Test
    void allTransactionsByAccountSuccessfullTest() {
        //Given
        Account account = entityManager.persistFlushFind(new Account("sunita"));
        entityManager.persistAndFlush(new Transaction(account, new BigDecimal("100"), "initial credit", Currency.GBP, LocalDate.now().minusDays(1)));
        entityManager.persistAndFlush(new Transaction(account, new BigDecimal("200"), "additional credit", Currency.GBP, LocalDate.now()));

        //When
        Page<Transaction> transactionPage = transactionRepository.findByAccount(account, PageRequest.of(0, 5));
        // NOTE: page start w/ 0 by default. we can change, but i have not since UI can take care of this

        //Then
        assertThat(transactionPage.getTotalPages()).isEqualTo(1);
        assertThat(transactionPage.getTotalElements()).isEqualTo(2);
        assertThat(transactionPage.getContent()).hasSize(2);

        assertThat(transactionPage.getContent().stream().map(Transaction::getDescription)).containsExactly("initial credit", "additional credit");
        assertThat(transactionPage.getContent().stream().map(Transaction::getTransactionAmount)).containsExactly(new BigDecimal("100"), new BigDecimal("200"));
        assertThat(transactionPage.getContent().stream().map(Transaction::getCurrency)).allMatch(c -> c.equals(Currency.GBP));
        assertThat(transactionPage.getContent().stream().map(Transaction::getAccount).map(Account::getUsername)).allMatch(u -> u.equals("sunita"));
    }

    @Test
    void allTransactionsByAccountWithNoTransactionTest() {
        //Given no transaction
        Account account = entityManager.persistFlushFind(new Account("sunita"));

        //When
        Page<Transaction> transactionPage = transactionRepository.findByAccount(account, PageRequest.of(0, 5));

        //Then
        assertThat(transactionPage.getTotalPages()).isZero();
        assertThat(transactionPage.getTotalElements()).isZero();
        assertThat(transactionPage.getContent()).isEmpty();
    }

    @Test
    void balanceTest() {
        //Given
        Account account = entityManager.persistFlushFind(new Account("sunita"));
        entityManager.persistAndFlush(new Transaction(account, new BigDecimal("100.43"), "initial credit", Currency.GBP, LocalDate.now().minusDays(1)));
        entityManager.persistAndFlush(new Transaction(account, new BigDecimal("200.33"), "additional credit", Currency.GBP, LocalDate.now()));

        //When
        List<Balance> balance = transactionRepository.findBalance(account.getId());

        //Then
        assertThat(balance).hasSize(1);
        assertThat(balance.get(0).getBalance()).isEqualTo(new BigDecimal("300.76"));
        assertThat(balance.get(0).getCurrencyCode()).isEqualTo(Currency.GBP.toString());
    }

}