**ASSUMPTIONS:**

Since POST /spend requires us to provide CURRENCY CODE and we can have multiple currency code(GBP/USD/EUR) and by
mistake user can enter different currency code during post call hence I'm returning List<Balance> for /balance endpoint
instead of just Balance. I'm grouping balance based on currency. Also, Currency Conversion has not been mentioned hence
not doing that to create single Balance

**RUN INSTRUCTIONS**

- mvnw spring-boot:run
- goto http://localhost:8080/swagger-ui.html
- execute /login which would provide 200 OK response code w/ response body json w/ key accessToken
- copy accessToken from above, click on authorize( lock icon ) and add bearer token
- now, we are ready to execute /spend /transactions /balance endpoint